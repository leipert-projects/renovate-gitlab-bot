# renovate-gitlab-bot

The bot uses [renovatebot](https://github.com/renovatebot/renovate) to automatically create MRs for updating dependencies.
It is running on a [tight schedule](https://gitlab.com/leipert-projects/renovate-gitlab-bot/pipeline_schedules).
The following repositories are currently being updated automatically.
For more info have a look at the config [config](./config.js).

<!-- rep -->

- [gitlab-com/teampage-map](https://gitlab.com/gitlab-com/teampage-map)
- [gitlab-org/gitlab-services/design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com)
- [gitlab-org/gitlab-svgs](https://gitlab.com/gitlab-org/gitlab-svgs)
- [gitlab-org/gitlab-ui](https://gitlab.com/gitlab-org/gitlab-ui)
- [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab)
- [leipert-projects/gettext-extractor-vue](https://gitlab.com/leipert-projects/gettext-extractor-vue)
- [leipert-projects/is-gitlab-pretty-yet](https://gitlab.com/leipert-projects/is-gitlab-pretty-yet)
- [leipert-projects/yarn-why-json](https://gitlab.com/leipert-projects/yarn-why-json)

<!-- rep -->
